package resources

import (
	"time"
	"math/rand"
	"fmt"
	"errors"
	"log"
	"strconv"
	"strings"
	"context"

	"github.com/Stoovles/ytdl"
)

// ResourceType enumeration indicating what kind
// of resource is indicated by the 'Raw' interface
type ResourceType uint16

// Enumeration Values
const (
	Raw ResourceType = iota
	String
	ResourceSlice
	ResourceFile
)

// Resource - wrapper struct for general-purpose resource loading
// if unsure of what type it is explicitly, then use the Type parameter
type Resource struct {
	Type ResourceType
	Raw  interface{}
}

const laughSubdir = "laugh/"

var errBadProvider = errors.New("there is nothing configured to load a resource with that URI")
var errBadURI = errors.New("invalid resource URI")
var errNoResource = errors.New("no such resource found")

const (
	internet = "https://"
	youtube  = "https://www.youtube.com"
	embed    = "embed://"
)

// LoadResource - funnel function to load a resource
// should be called via a well-formed uri e.g. embed://vote/human/0
// full scheme is:
//
//
//
//
//
//
func LoadResource(resURI string) (Resource, error) {
	switch {
	case strings.HasPrefix(resURI, embed):
		return loadEmbed(strings.Replace(resURI, embed, "", 1))
	case strings.HasPrefix(resURI, youtube):
		return loadYoutube(resURI)
	case strings.HasPrefix(resURI, internet):
	default:
		log.Println(errBadProvider)
		return Resource{}, errBadProvider
	}

	return Resource{}, errBadProvider
}

func loadEmbed(location string) (Resource, error) {
	switch {
	case strings.HasPrefix(location, titoSubdir):
		textVal, err := loadWisdom(strings.Replace(location, titoSubdir, "", 1))
		if err != nil {
			return Resource{}, err
		}

		f, err := embeddedFiles.Open(location)
		if err != nil {
			log.Println(err)
			return Resource{}, err
		}

		return Resource{Type: ResourceSlice, Raw: []Resource{
			{
				Type: String,
				Raw:  textVal,
			},
			{
				Type: ResourceFile,
				Raw:  f,
			},
		}}, nil

	case strings.HasPrefix(location, voteSubdir):
		stripped := strings.Replace(location, voteSubdir, "", 1)
		if strings.HasPrefix(stripped, humanReadable) {
			key, err := strconv.Atoi(strings.Replace(stripped, humanReadable, "", 1))
			if err != nil {
				return Resource{}, err
			}

			asset, err := loadEmojiEnglish(key)
			if err != nil {
				return Resource{}, err
			}

			return Resource{Type: String, Raw: asset}, nil

		} else if strings.HasPrefix(stripped, computerReadable) {
			key, err := strconv.Atoi(strings.Replace(stripped, computerReadable, "", 1))
			if err != nil {
				return Resource{}, err
			}

			asset, err := loadEmojiComputer(key)
			if err != nil {
				return Resource{}, err
			}

			return Resource{Type: String, Raw: asset}, nil
		} else {
			return Resource{}, errBadURI
		}

	case strings.HasPrefix(location, laughSubdir):
		f, err := embeddedFiles.Open(location)
		if err != nil {
			log.Println(err)
			return Resource{}, err
		}

		return Resource{Type: ResourceFile, Raw: f}, nil

	default:
		log.Printf("%v : %s", location, errBadProvider)
		return Resource{}, errBadProvider
	}
}

// LoadRandomTitoResource - load a random resource from the tito subdirectory
// used in inspire
func LoadRandomTitoResource() (Resource, error) {
	rand.Seed(time.Now().UnixNano())
	arr := make([]string, 0)
	for k := range ancientWisdom {
		arr = append(arr, k)
	}

	key := arr[rand.Intn(len(ancientWisdom))]
	uri := fmt.Sprintf("embed://tito/%s", key)
	return LoadResource(uri)
}

func loadYoutube(url string) (Resource, error) {
	videoInfo, err := ytdl.DefaultClient.GetVideoInfo(context.Background(), url)
	if err != nil {
		log.Println(err)
		return Resource{}, err
	}

	format := videoInfo.Formats.Extremes(ytdl.FormatAudioBitrateKey, true)[0]
	downloadURL, err := ytdl.DefaultClient.GetDownloadURL(context.Background(), videoInfo, format)
	if err != nil {
		log.Println(err)
		return Resource{}, err
	}

	return Resource{Type: String, Raw: downloadURL.String()}, nil
}
