// +build !embed

package resources

import (
	"bytes"
	"os"

	"github.com/omeid/go-resources/live"
)

// Dictionary is a live version of the normally embedded resources.
// it serves directly from the fs.
var embeddedFiles = live.Dir("./")

type File struct {
	*bytes.Reader
	data []byte
	fi   os.FileInfo
}
