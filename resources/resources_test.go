package resources

import (
	"errors"
	"strings"
	"testing"

	"gitlab.com/the-tito-foundation/tito/logging"
)

var errAssertFail = errors.New("assert failure")

const HappyJinjoHouse = "https://www.youtube.com/watch?v=Kxcfk2mMHxQ"
const NotHappyJinjos = "https://www.youtube.com/"

func TestTextResourceLoading(t *testing.T) {
	logging.StartLogging("")
	t.Run("Wisdom Text", WisdomLoadingTest)
	t.Run("Emoji-Human Readable", EmojiHumanTest)
	t.Run("Emoji-Unicode", EmojiUnicodeTest)
}

func TestYoutubeResourceLoading(t *testing.T) {
	logging.StartLogging("")
	res, err := loadYoutube(HappyJinjoHouse)
	if err != nil {
		t.Error(err)
	}

	if res.Type != String {
		t.Error(errAssertFail)
	}
	// Download URI is dynamic, can't hardcode and validate results

	res, err = loadYoutube(NotHappyJinjos)
	if err == nil {
		t.Error(err)
	}
}

func TestLoadResource(t *testing.T) {
	logging.StartLogging("")
	t.Run("Embed Testing", EndpointEmbedTest)
	t.Run("YouTube Testing", YoutubeEndpointTest)
	t.Run("Internet/No Provider", func(t *testing.T) {
		_, err := LoadResource("https://www.google.com")
		if err == nil {
			t.Error(errAssertFail)
		}

		_, err = LoadResource("abc://")
		if err == nil {
			t.Error(errAssertFail)
		}
	})
}

func YoutubeEndpointTest(t *testing.T) {
	logging.StartLogging("")
	res, err := LoadResource(HappyJinjoHouse)
	if err != nil {
		t.Error(err)
	}

	if res.Type != String {
		t.Error(errAssertFail)
	}
	// Download URI is dynamic, can't hardcode and validate results

	res, err = LoadResource(NotHappyJinjos)
	if err == nil {
		t.Error(err)
	}
}

func EndpointEmbedTest(t *testing.T) {
	res, err := LoadResource("embed://vote/human/0")
	if err != nil {
		t.Error(err)
	}

	if res.Type != String || strings.Compare(res.Raw.(string), ":zero:") != 0 {
		t.Error(errAssertFail)
	}

	res, err = LoadResource("embed://vote/computer/0")
	if err != nil {
		t.Error(err)
	}

	if res.Type != String || strings.Compare(res.Raw.(string), "\u0030\u20E3") != 0 {
		t.Error(errAssertFail)
	}

	res, err = LoadResource("embed://laugh/laugh.ogg")
	if err != nil {
		t.Error(errAssertFail)
	}

	res, err = LoadResource("embed://laugh/not-a-laugh.ogg")
	if err == nil {
		t.Error(errAssertFail)
	}

	res, err = LoadResource("embed://not-a-subdir")
	if err == nil {
		t.Error(errAssertFail)
	}

	res, err = LoadResource("embed://vote/not-an-option")
	if err == nil {
		t.Error(errAssertFail)
	}

	res, err = LoadResource("embed://vote/human/not-an-option")
	if err == nil {
		t.Error(errAssertFail)
	}

	res, err = LoadResource("embed://vote/computer/not-an-option")
	if err == nil {
		t.Error(errAssertFail)
	}

	res, err = LoadResource("embed://vote/computer/11")
	if err == nil {
		t.Error(errAssertFail)
	}

	res, err = LoadResource("embed://vote/human/-1")
	if err == nil {
		t.Error(errAssertFail)
	}

	res, err = LoadResource("embed://tito/not-a-file.ogg")
	if err == nil {
		t.Error(err)
	}

	res, err = LoadResource("embed://tito/S1E02A.ogg")
	if err != nil {
		t.Error(err)
	}

	if res.Type != ResourceSlice && len(res.Raw.([]Resource)) != 2 {
		t.Error(errAssertFail)
	}

	slc := res.Raw.([]Resource)
	if slc[0].Type != String && strings.Compare(slc[0].Raw.(string), "The mouth cannot speak what the mind does not leak. Tomorrow, you'll lead your friends to the spot, but say its location, you shall not") != 0 {
		t.Error(errAssertFail)
	}

	if slc[1].Type != ResourceFile {
		t.Error(errAssertFail)
	}
}

func WisdomLoadingTest(t *testing.T) {
	for key, v := range ancientWisdom {
		if loadedV, err := loadWisdom(key); loadedV != v && err != nil {
			t.Error(err)
		}
	}

	_, err := loadWisdom("abc")
	if err == nil {
		t.Error()
	}
}

func EmojiHumanTest(t *testing.T) {
	for key, v := range emojiVote {
		if loadedV, err := loadEmojiEnglish(key); loadedV != v && err != nil {
			t.Error(err)
		}
	}

	_, err := loadEmojiEnglish(10)
	if err == nil {
		t.Error()
	}

	_, err = loadEmojiEnglish(-1)
	if err == nil {
		t.Error()
	}
}

func EmojiUnicodeTest(t *testing.T) {
	for key, v := range emojiUnicode {
		if loadedV, err := loadEmojiComputer(key); loadedV != v && err != nil {
			t.Error(err)
		}
	}

	_, err := loadEmojiComputer(10)
	if err == nil {
		t.Error()
	}

	_, err = loadEmojiComputer(-1)
	if err == nil {
		t.Error()
	}
}
