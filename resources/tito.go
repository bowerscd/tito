package resources

const titoSubdir = "tito/"

var ancientWisdom = map[string]string{
	"S1E02A.ogg":                      "The mouth cannot speak what the mind does not leak. Tomorrow, you'll lead your friends to the spot, but say its location, you shall not",
	"S1E04A.ogg":                      "No common brudda ever knows the pain of the flower as it grows.",
	"S1E05B - Needs Edit.ogg":         "When surfers don't ride, potatoes ain't fried.",
	"S1E06A.ogg":                      "Oh no little cuz, the Hawaiian ancients had an expression for someone who competes against his best friend and business partner: real bonehead.",
	"S1E06B-1.ogg":                    "(Something in Hawaiian?) Ray: Ain’t it the truth. Wait, what does that mean? Tito: More barbecue for us brudda.",
	"S1E06B-2.ogg":                    "Just because you can afford the modern longboard, doesn't mean you can honor the moving water.",
	"S1E07A.ogg":                      "Never provoke the wrath of a ten year old",
	"S1E07B.ogg":                      "If you're walking on a beach, and you step on a crab, even if you didn't mean to, you should go back and apologize. After all, that crab could be in your soup later on.",
	"S1E09B.ogg":                      "The coconut, though hard and brown, in the spring gives milk to the sand, after being kissed by the green water.",
	"S1E11A - Needs Edit.ogg":         "The most important races are won in the ocean of the soul.",
	"S1E12A - 1 - Needs Edit.ogg":     "Sometimes, the bird sings, sometimes it coughs up a worm.",
	"S1E12A - 2.ogg":                  "Sometimes, it's best to trust the weight of experience, and I've got plenty of experience!",
	"S1E12A - 3 - Needs Edit.ogg":     "Fly the kite; it must not fly you.",
	"S1E14A - Needs Edit.ogg":         "(Something ih Hawaiian?) Sam: What’s that mean? Tito: Relax and have a killer time cuz.",
	"S1E16A.ogg":                      "There is no I in the word team.",
	"S1E18A.ogg":                      "Keoni: Some days, your brudda might need your shirt — dry cleaned, wrinkled or stained — you gotta give it to them, because some day brah, you might need theirs. Tito: Actually it was, aahhh, close enough.",
	"S1E19A - Needs Edit.ogg":         "Don't do the crime, if you can't do the time.",
	"S1E19B.ogg":                      "The bird that flies alone, is the goose that gets cooked.",
	"S1E20B.ogg":                      "The coconut is always sweeter on the palm tree you can't reach.",
	"S1E22B.ogg":                      "Tito: Don't do anything to your brudda… Twister: That you wouldn't want your brudda to do to you?",
	"S2E04B.ogg":                      "Lie about the size, I can see it in your eyes.",
	"S2E05B - 1.ogg":                  "The seagull that flies a crooked path, has a tough time returning home.",
	"S2E05B - 2.ogg":                  "When you're itching for the waves, the only lotion is the ocean.",
	"S2E07A - Possible Edit.ogg":      "When the sea turtle retreats into its hardened shell, just give him time  he'll show up, especially when there's teriyaki barbecue chicken.",
	"S2E07B.ogg":                      "He who carves himself in stone, has rocks in his head.",
	"S2E12B - 1 - Possbile Edits.ogg": "Blood is thicker than water, but not as refreshing.",
	"S2E12B - 2.ogg":                  "The sands of the beach must change with each new wind.",
	"S2E16B.ogg":                      "The pelican can fly as high as the stars, if she believes she can.",
	"S3E1B.ogg":                       "You can't compare pineapples and coconuts.",
	"S3E3B.ogg":                       "Once a pineapple is ripe, it's very hard to make it a seed again.",
	"Special1.ogg":                    "The village of a chief who won't forget the past, may as well forget about the future.",
	"S3E4A.ogg":                       "If your lingo's cool and freaky, then everybody will want to speaky… It.",
	"S3E5B.ogg":                       "The green pineapple never falls from the tree by accident.",
	"S3E6A.ogg":                       "The sun cannot drink its favorite pineapple in the field.",
	"S3E7A.ogg":                       "Man who's always watching for fallen coconut ends up stubbing toe.",
	"S3E7B.ogg":                       "A crashing wave may thrill the breath, but can't replace the ocean depth.",
	"S3E8A.ogg":                       "He who rubs his enemy's face in the sand, gets buried in the sand with him.",
	"S3E8B.ogg":                       "He who is on the canoe is responsible for the journey, whether he has a paddle or not.",
	"S3E9A - 1.ogg":                   "Sometimes a pineapple wishes it was a coconut.",
	"S3E9A - 2.ogg":                   "Never drink the milk from a coconut you found in the dark.",
	"S3E9A - 3.ogg":                   "Some coconuts have thicker shells than others.",
	"S3E9B - 1.ogg":                   "Every wave has to come to the shore at some point.",
	"S3E9B - 2.ogg":                   "You can paint the coconut shell, but you can't change the milk inside.",
	"S3E10A.ogg":                      "A coconut is a hard nut to crack. Then again, it takes something hard to crack the coconut.",
	"S3E11A.ogg":                      "Don't bite the head off a piranha or you might get chewed.",
	"S3E12A.ogg":                      "Inside every rotten pineapple, there may be a pearl.",
	"S3E12B.ogg":                      "Sometimes the tastiest coconut is found on the shortest tree.",
	"S3E15 - 1.ogg":                   "Hold the pickle.",
	"S3E15 - 2 - Needs Edits.ogg":     "If there is anyone in the village who can understand why the new chief made the mistake of planting coconuts near the volcano, it's the old chief.",
	"S3E16.ogg":                       "But first, I think you need to figure that one out for yourself.",
	"S3E17B.ogg":                      "The only real spooky stuff, is spooky stuff, that is real spooky.",
	"S3E18A.ogg":                      "Just 'cause you're riding the high tide, doesn't mean you've chosen the right course.",
	"S3E18B.ogg":                      "Man who tries to predict when volcano will blow, is usually toast by next day.",
	"S3E20.ogg":                       "He who misses the tide, must wait for the next moon.",
	"Special3.ogg":                    "A wise man cherishes today because tomorrow he could meet the fiery end at the bottom of a volcano.",
}

func loadWisdom(key string) (string, error) {
	v, ok := ancientWisdom[key]
	var err error
	if ok {
		err = nil
	} else {
		err = errNoResource
	}
	return v, err
}
