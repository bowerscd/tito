package logging

import (
	"log"
	"os"
	"strings"
)

var fileHandle *os.File

// StartLogging - start the logging. If filepath is the empty string, then
// it will log to stderr, otherwise it will log to the filepath indicated by
// the passed parameter
func StartLogging(filepath string) error {
	log.SetFlags(log.Ldate | log.Ltime | log.Llongfile)

	fileHandle, err := os.OpenFile(filepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		log.SetOutput(os.Stderr)
		if strings.Compare(filepath, "") != 0 {
			log.Println(err)
		}
		return err
	}

	log.SetOutput(fileHandle)

	return nil
}

// StopLogging - stop the logging, closing the open file handle created by StartLogging
// if necessary
func StopLogging() {
	if fileHandle != nil {
		fileHandle.Close()
	}
}
