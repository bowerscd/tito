package logging

import (
	"bufio"
	"log"
	"os"
	"strings"
	"testing"
)

const TestFileName = "TitoTestLog.log"

func TestFileLogging(t *testing.T) {
	os.Remove(TestFileName)

	t.Run("Logging E2E (File)", func(t *testing.T) {

		err := StartLogging(TestFileName)

		logOutput := []string{"!", ""}

		if err != nil {
			t.Fail()
		}
		// prevent pesky timestamps et all
		log.SetFlags(0)
		log.Printf("!")
		StopLogging()

		f, err := os.OpenFile(TestFileName, os.O_RDONLY, 0644)
		if err != nil {
			t.Error(err)
		}

		sc := bufio.NewScanner(f)
		i := 0
		for sc.Scan() {

			if len(logOutput) >= i {
			}

			if strings.Compare(sc.Text(), logOutput[i]) != 0 {
				t.Fail()
			}

			if sc.Err() != nil {
				t.Error(err)
			}

			i++
		}
	})
}

func TestStdoutLoging(t *testing.T) {
	t.Run("Logging E2E (Stdout)", func(t *testing.T) {

		err := StartLogging("")

		if err == nil {
			t.Fail()
		}
		// prevent pesky timestamps et all
		log.SetFlags(0)
		log.Printf("!")
		StopLogging()
	})
}
