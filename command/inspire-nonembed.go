//+build !embed

package command

import (
	"log"
	"os"

	"github.com/jonas747/dca"
	"gitlab.com/the-tito-foundation/tito/resources"
)

// Inspire - unembedded inspire, play from an external resource (TBD)
func Inspire() ([]Response, error) {

	res, err := resources.LoadRandomTitoResource()
	if err != nil {
		return []Response{}, err
	}

	text := res.Raw.([]resources.Resource)[0]
	file := res.Raw.([]resources.Resource)[1].Raw.(*os.File)

	options := dca.StdEncodeOptions
	options.RawOutput = true
	options.Bitrate = 96
	options.Application = "lowdelay"

	encodingSession, err := dca.EncodeMem(file, options)
	if err != nil {
		log.Println(err)
		return []Response{}, err
	}


	response := Response{
		ShouldRespondToChannel: true,
		ShouldRespondToUser:    false,
		ShouldSendVoice:        true,
		VoiceOutput:            encodingSession,
		Message:                text.Raw.(string),
	}

	return []Response{response}, nil

}
