package command

import (
	"gitlab.com/the-tito-foundation/tito/database"
)

// Deop - demote a player to MEMBER
func Deop(discordID string) ([]Response, error) {
	u := database.User{DiscordID: discordID}

	inDb, err := database.SelectUser(u)
	if err != nil {
		return []Response{}, err
	}

	if inDb.Role > database.MEMBER {
		u.Role = database.MEMBER
		err := database.UpdateUser(u)
		if err != nil {
			return []Response{}, err
		}
	}

	return []Response{}, nil
}
