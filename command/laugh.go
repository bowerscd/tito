package command

import (
	"log"
	"os"

	"github.com/jonas747/dca"
	"gitlab.com/the-tito-foundation/tito/resources"
)

const laughURI = "embed://laugh/laugh.ogg"

// Laugh - play a crazy ohjousama laugh
func Laugh() ([]Response, error) {

	res, err := resources.LoadResource(laughURI)
	if err != nil {
		return []Response{}, err
	}

	f := res.Raw.(*os.File)
	options := dca.StdEncodeOptions
	options.RawOutput = true
	options.Bitrate = 96
	options.Application = "lowdelay"

	encodingSession, err := dca.EncodeMem(f, options)
	if err != nil {
		log.Println(err)
		return []Response{}, err
	}

	response := Response{
		ShouldSendVoice: true,
		VoiceOutput:     encodingSession,
	}

	return []Response{response}, nil

}
