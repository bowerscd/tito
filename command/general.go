package command

import (
	"errors"

	"github.com/jonas747/dca"
	"gitlab.com/the-tito-foundation/tito/database"
)

// Response - struct indicating what the bot should do after a command is executed
type Response struct {
	ShouldRespondToUser    bool
	ShouldRespondToChannel bool
	ShouldSendVoice        bool

	VoiceOutput *dca.EncodeSession

	Message        string
	ReactionsToAdd []string
}

var errDoesNotExist = errors.New("object does not exist in database")
var errNoOptions = errors.New("no options specified")

// ProgrammedCommands - commands for lookup whether a string is a valid command or not
var ProgrammedCommands = map[string]bool{
	AddGameCmd: true,
	DeopCmd:    true,
	DisableCmd: true,
	GamesCmd:   true,
	EnableCmd:  true,
	HelpCmd:    true,
	InspireCmd: true,
	LaughCmd:   true,
	OpCmd:      true,
	PlayCmd:    true,
	PollCmd:    true,
	RegCmd:     true,
	SourceCmd:  true,
	WhoIsCmd:   true,
	SummonCmd: true,
}

// PermissionList - Permissions for individual commands
// acts as a "must have AT LEAST" this role,
// meaning anyone with a higher role will be
// able to use it.
var PermissionList = map[string]database.UserRole{
	AddGameCmd: database.MEMBER,
	DeopCmd:    database.ADMIN,
	DisableCmd: database.OFFICER,
	GamesCmd:   database.MEMBER,
	EnableCmd:  database.OFFICER,
	HelpCmd:    database.INACTIVE,
	InspireCmd: database.REGISTERED,
	LaughCmd:   database.REGISTERED,
	OpCmd:      database.ADMIN,
	PlayCmd:    database.REGISTERED,
	PollCmd:    database.REGISTERED,
	RegCmd:     database.OFFICER,
	SourceCmd:  database.INACTIVE,
	WhoIsCmd:   database.MEMBER,
	SummonCmd: 	database.MEMBER,
}

var helpList = map[database.UserRole]map[string]string{
	database.INACTIVE: {
		HelpCmd:   "Usage: !help\n\tDisplays this Message",
		SourceCmd: "Usage: !source\n\tDisplays the repository of Tito",
	},
	database.UNREGISTERED: {},
	database.REGISTERED: {
		PollCmd:    "Usage: !poll <option1> <option2> ... <option n>\n\tSends a message to a channel, giving reaction options to count as a poll",
		PlayCmd:    "Usage: !play <youtube url>\n\tPlays the indicated YoutubeURL over Tito. Does not support queueing",
		InspireCmd: "Usage: !inspire\n\tPlays a random ancient hawaiian wisdom from Tito's great knowledge",
		LaughCmd:   "Usage: !laugh\n\tInvokes the power of the hundreds of years of anime to give an ear-shattering laugh",
	},
	database.MEMBER: {
		WhoIsCmd:   "Usage: !whois <UserNickname>\n\tShows the registered real name of the individual",
		GamesCmd:   "Usage: !games <NumberOfPlayers>\n\tShows what games and who owns the game(s) for the indicated player count",
		AddGameCmd: "Usage: !addgame <GameName> <MaxPlayers>\n\tRegister the <GameName> with Maximum PlayerCount <MaxPlayers> in the games DB",
		SummonCmd: "Usage: !summon @<Username>\n\t@ mentions the individual three times",
	},
	database.OFFICER: {
		DisableCmd: "Usage: !disable <UserNickName>\n\tDisables a user's ability to use Tito",
		EnableCmd:  "Usage: !enable <UserNickName>\n\tEnable a user's ability to use Tito",
		RegCmd:     "Usage: !reg <UserNickName> <RealName>\n\tRegistes a <UserNickName> as <RealName>",
	},
	database.ADMIN: {
		DeopCmd: "Usage: !op <UserNickname>\n\tPromotes <UserNickname> to Admin",
		OpCmd:   "Usage: !deop <UserNickname>\n\tDemotes <UserNickname> to Admin",
	},
	database.OWNER: {},
}

// AddGameCmd - constant indicating the string the command should be
const AddGameCmd = "addgame"

// DeopCmd - constant indicating the string the command should be
const DeopCmd = "deop"

// DisableCmd - constant indicating the string the command should be
const DisableCmd = "disable"

// GamesCmd - constant indicating the string the command should be
const GamesCmd = "games"

// EnableCmd - constant indicating the string the command should be
const EnableCmd = "enable"

// HelpCmd - constant indicating the string the command should be
const HelpCmd = "help"

// InspireCmd - constant indicating the string the command should be
const InspireCmd = "inspire"

// LaughCmd - constant indicating the string the command should be
const LaughCmd = "laugh"

// OpCmd - constant indicating the string the command should be
const OpCmd = "op"

// PlayCmd - constant indicating the string the command should be
const PlayCmd = "play"

// PollCmd - constant indicating the string the command should be
const PollCmd = "poll"

// RegCmd - constant indicating the string the command should be
const RegCmd = "reg"

// SourceCmd - constant indicating the string the command should be
const SourceCmd = "source"

// WhoIsCmd - constant indicating the string the command should be
const WhoIsCmd = "whois"

// SummonCmd - constant indicating the string the command should be
const SummonCmd = "summon"
