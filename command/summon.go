package command

import (
	"fmt"

)
// Summon - mentions (@<user>) three times.
func Summon(mentionTag string) ([]Response, error) {

	response := Response{
		ShouldRespondToChannel:	true,
		ShouldRespondToUser:		false,
		Message:								fmt.Sprintf(mentionTag),
	}

	return []Response{response, response, response}, nil

}
