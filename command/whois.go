package command

import (
	"fmt"

	"gitlab.com/the-tito-foundation/tito/database"
)

// WhoIs - show the indicated user's real name.
func WhoIs(discordID string) ([]Response, error) {
	u := database.User{DiscordID: discordID}

	u, err := database.SelectUser(u)
	if err != nil {
		return []Response{}, err
	}


	response := Response{
		ShouldRespondToChannel: false,
		ShouldRespondToUser:    true,
		Message:                fmt.Sprintf(fmt.Sprintf("User %s is %s", u.Nickname, u.RealName)),
	}

	return []Response{response}, nil

}
