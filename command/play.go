package command

import (
	"log"

	"github.com/jonas747/dca"
	"gitlab.com/the-tito-foundation/tito/resources"
)

// Play - play the indicated youtube url through Tito.
func Play(uri string) ([]Response, error) {
	res, err := resources.LoadResource(uri)
	if err != nil {
		return []Response{}, err
	}

	options := dca.StdEncodeOptions
	options.RawOutput = true
	options.Bitrate = 96
	options.Application = "lowdelay"

	downloadURI := res.Raw.(string)

	encodingSession, err := dca.EncodeFile(downloadURI, options)
	if err != nil {
		log.Println(err)
		return []Response{}, err
	}

	response := Response{
		ShouldSendVoice: true,
		VoiceOutput:     encodingSession,
	}

	return []Response{response}, nil

}
