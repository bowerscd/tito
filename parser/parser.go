package parser

import (
	"errors"
	"log"
	"regexp"
)

// ParseCli parses the given string into tokens
// condensing tokens as necessary if they are using double quotes or single quotes
// as a secondary step, it will perform environment variable substitutions
func ParseCli(unparsed string) ([]string, error) {
	whitesp, err := regexp.Compile("\\s+")

	if err != nil {
		log.Println(err)
		return nil, err
	}

	var argv []string

	for i := 0; i < len(unparsed); i++ {
		var token string

		if unparsed[i] == '"' {
			i++

			for i < len(unparsed) && unparsed[i] != '"' {
				token += string(unparsed[i])
				i++
			}

			if i == len(unparsed) {
				err := errors.New("ParseError: Reached EOF While Parsing, did you forget \" ?")
				log.Println(err)
				return nil, err
			}

			argv = append(argv, token)
		} else if unparsed[i] == '\'' {
			i++

			for i < len(unparsed) && unparsed[i] != '\'' {
				token += string(unparsed[i])
				i++
			}

			if i == len(unparsed) {
				err := errors.New("ParseError: Reached EOF While Parsing, did you forget \" ?")
				log.Println(err)
				return nil, err
			}

			argv = append(argv, token)
		} else if whitesp.Match([]byte(string(unparsed[i]))) {
			i++

			for i < len(unparsed) && whitesp.Match([]byte(string(unparsed[i]))) {
				i++
			}

			// rewind so we do not skip the next character
			i--
		} else {
			for i < len(unparsed) && !whitesp.Match([]byte(string(unparsed[i]))) {
				token += string(unparsed[i])
				i++
			}

			argv = append(argv, token)
		}
	}

	return argv, nil
}
