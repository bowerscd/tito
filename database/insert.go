package database

import (
	"errors"
	"log"
)

var errAlreadyExists = errors.New("attempting to create a database object where one already exists")

// InsertUser - Add a user.
func InsertUser(user User) error {
	user.Games = nil
	user.Messages = nil

	b, err := IsInUserDB(user)
	if b {
		log.Println(errAlreadyExists)
		return errAlreadyExists
	} else if err != nil {
		return err
	}

	err = dbConn.Create(&user).Error
	return err
}

// InsertGame - Add a game.
func InsertGame(game Game) error {
	game.Owners = nil

	b, err := IsInGameDB(game)
	if b {
		log.Println(errAlreadyExists)
		return errAlreadyExists
	} else if err != nil {
		return err
	}

	err = dbConn.Create(&game).Error
	return err
}

// InsertMessage - Add a message.
func InsertMessage(message Message) error {
	message.Senders = nil

	b, err := IsInMsgDB(message)
	if b {
		log.Println(errAlreadyExists)
		return errAlreadyExists
	} else if err != nil {
		return err
	}

	err = dbConn.Create(&message).Error

	return err
}

// InsertUserMessageAssoc - insert an association in the table between
// the message and the user passed in
func InsertUserMessageAssoc(message Message, user User) error {
	msg, err := SelectMessage(message)
	if err != nil {
		return err
	}

	u, err := SelectUser(user)
	if err != nil {
		return err
	}

	u.Messages = nil
	u.Games = nil
	msg.Senders = nil

	return dbConn.Model(&msg).Association("Senders").Append(&u).Error
}

// InsertGameUserAssoc - insert an association in the table between
// the game and the user passed in
func InsertGameUserAssoc(game Game, user User) error {
	g, err := SelectGame(game)
	if err != nil {
		return err
	}

	u, err := SelectUser(user)
	if err != nil {
		return err
	}
	u.Games = nil
	u.Messages = nil
	game.Owners = nil

	return dbConn.Model(&g).Association("Owners").Append(&u).Error
}
