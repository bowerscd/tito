package database

import (
	"log"

	"github.com/jinzhu/gorm"
)

// IsInGameDB - checks if a game exists in the database
func IsInGameDB(game Game) (bool, error) {
	_, err := SelectGame(game)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return false, nil
		}
		return false, err
	}
	return true, err
}

// IsInUserDB - checks if a user exists in the database
func IsInUserDB(user User) (bool, error) {
	_, err := SelectUser(user)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// IsInMsgDB - checks if a message exists in the database
func IsInMsgDB(msg Message) (bool, error) {
	_, err := SelectMessage(msg)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return false, nil
		}
		return false, err
	}
	return true, err
}

// SelectGame - return a game Object in the database
func SelectGame(game Game) (Game, error) {
	dbObj := Game{}
	dbObj.Owners = nil
	game.Owners = nil

	err := dbConn.Preload("Owners").Where(&game).First(&dbObj).Error
	if err != nil {
		log.Println(err)
	}
	return dbObj, err
}

// SelectGamesForPlayerCount - Select the games objects that have at least the player count indicated
func SelectGamesForPlayerCount(PlayerCount uint8) ([]Game, error) {
	games := []Game{}
	err := dbConn.Preload("Owners").Where("max_players >= ?", PlayerCount).Find(&games).Error
	if err != nil {
		log.Println(err)
	}
	return games, err
}

// SelectUser - return a User Object in the database by the primary key, discordID
func SelectUser(user User) (User, error) {
	dbObj := User{DiscordID: user.DiscordID}
	dbObj.Messages = nil
	dbObj.Games = nil

	user.Games = nil
	user.Messages = nil

	err := dbConn.Where(&user).First(&dbObj).Error
	if err != nil {
		log.Println(err)
	}
	return dbObj, err
}

// SelectUserByUsername - select the first user that has the username indicated
func SelectUserByUsername(username string) (User, error) {
	dbObj := User{}
	dbObj.Messages = nil
	dbObj.Games = nil

	err := dbConn.Preload("Games").Where(User{Username: username}).First(&dbObj).Error
	if err != nil {
		log.Println(err)
	}
	return dbObj, err
}

// SelectUserByNickname - select the first user that has the nickname indicated
func SelectUserByNickname(nickname string) (User, error) {
	dbObj := User{}

	dbObj.Messages = nil
	dbObj.Games = nil

	err := dbConn.Preload("Games").Where(User{Nickname: nickname}).First(&dbObj).Error
	if err != nil {
		log.Println(err)
	}
	return dbObj, err
}

// SelectMessage - return a Message Object in the database
func SelectMessage(msg Message) (Message, error) {
	dbObj := Message{}

	dbObj.Senders = nil
	msg.Senders = nil

	err := dbConn.Preload("Senders").Where(&msg).First(&dbObj).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		log.Println(err)
	}
	return dbObj, err
}
