package database

import (
	"errors"
	"log"

	"github.com/jinzhu/gorm"

	// Dialect imports
	_ "github.com/jinzhu/gorm/dialects/mssql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var errConnExists = errors.New("Attempting to Close Database that Isn't Open")

// DBType - enum for database types supported out-of-the-box by gorm
type DBType uint8

// Definition of DBType Enum
const (
	SQLITE DBType = iota
	MYSQL
	MARIADB
	POSTGRESQL
	SQLSERVER
)

// Internal DB Connection to be used by the database package
var dbConn *gorm.DB

// ConnectToDB - Initialize and Start Database Connection
func ConnectToDB(dbType DBType, databaseURI string) error {
	var err error
	var dbConst string
	switch dbType {
	case SQLITE:
		dbConst = "sqlite3"
		break
	case MYSQL:
	case MARIADB:
		dbConst = "mysql"
		break
	case POSTGRESQL:
		dbConst = "postgres"
		break
	case SQLSERVER:
		dbConst = "mssql"
		break
	default:
		err := errors.New("Invalid Database Type")
		log.Println(err)
		return err
	}
	dbConn, err = gorm.Open(dbConst, databaseURI)
	if err != nil {
		log.Println(err)
		return err
	}

	dbConn.AutoMigrate(&User{}, &Game{}, &Message{})

	return nil

}

// CloseDB - Close Database Connection
func CloseDB() error {
	if dbConn == nil {
		log.Println(errConnExists)
		return errConnExists
	}
	err := dbConn.Close()
	dbConn = nil
	return err
}
