PROJECT_NAME := "tito"
PKG := "gitlab.com/the-tito-foundation/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/ | grep -v /resources)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go | grep -v /resources)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}
	find ./ -name "*.log" -exec rm "{}" \;
	find ./ -name "*.json" -exec rm "{}" \;

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}
	find ./ -name "*.log" -exec rm "{}" \;
	find ./ -name "*.json" -exec rm "{}" \;

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}
	find ./ -name "*.log" -exec rm "{}" \;
	find ./ -name "*.json" -exec rm "{}" \;

coverage: ## Generate global code coverage report
	./build/coverage.sh;
	find ./ -name "*.log" -exec rm "{}" \;
	find ./ -name "*.json" -exec rm "{}" \;

coverhtml: ## Generate global code coverage report in HTML
	./build/coverage.sh html;
	find ./ -name "*.log" -exec rm "{}" \;
	find ./ -name "*.json" -exec rm "{}" \;

dep: ## Get the dependencies
	@go get -v -d ./...

build: dep ## Build the binary file
	@go build -i -v $(PKG)

build-embed: dep
	@go build -i -v -tags embed $(PKG)

clean: ## Remove previous build
	find ./ -name "*.log" -exec rm "{}" \;
	find ./ -name "*.json" -exec rm "{}" \;
	@rm -f $(PROJECT_NAME)

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
