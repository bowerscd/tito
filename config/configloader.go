package config

import (
	"encoding/json"
	"errors"
	"log"
	"os"

	"gitlab.com/the-tito-foundation/tito/database"
)

// GuildOptions - options scoped to the guild the bot has residence in
type GuildOptions struct {
	GuildID         string
	GuildName       string
	TextChannel     string
	BotVoiceChannel string
}

// BotOptions - global options specifically related to the bot
type BotOptions struct {
	BotSecretKey string

	Mute   bool
	Deafen bool
}

// DatabaseOptions - options specifically related to the database of the bot
type DatabaseOptions struct {
	DatabaseType             database.DBType
	DatabaseConnectionString string
}

// OwnerOptions - options specifically related to the owner of the bot
type OwnerOptions struct {
	OwnerDiscordID string
}

// Configuration - struct that represents the top level JSON schema
type Configuration struct {
	OwnerConfig    OwnerOptions
	DatabaseConfig DatabaseOptions
	BotConfig      BotOptions
	GuildConfig    GuildOptions
}

const defaultDB = "default.sqlite3"
const defaultDBType = database.SQLITE
const cfgFileName = "TitoConfig.json"

var errAlreadyConfigured = errors.New("config already loaded")
var errNeedConfigure = errors.New("configuration is incomplete")

// Cfg - global indicating loaded configuration
var Cfg *Configuration

// LoadConfig - load tito's configuration from the JSON text file
func LoadConfig() error {
	Cfg = new(Configuration)

	var b []byte

	fd, err := os.Open(cfgFileName)
	if err != nil && !os.IsExist(err) {

		fd, err = os.Create(cfgFileName)
		if err != nil {
			log.Println(err)
			return err
		}
		defer fd.Close()

		b, err := json.MarshalIndent(Cfg, "", "  ")
		if err != nil {
			log.Println(err)
			return err
		}

		_, err = fd.Write(b)
		if err != nil {
			log.Println(err)
			return err
		}

		log.Println(errNeedConfigure)
		return errNeedConfigure
	}
	defer fd.Close()

	fInfo, err := fd.Stat()
	if err != nil {
		log.Println(err)
		return err
	}

	if fInfo.Size() != 0 {

		b = make([]byte, fInfo.Size())

		_, err = fd.Read(b)
		if err != nil {
			log.Println(err)
			return err
		}

		err = json.Unmarshal(b, Cfg)
		if nil != err {
			log.Println(err)
			return err
		}
	}

	if len(Cfg.BotConfig.BotSecretKey) == 0 ||
		len(Cfg.GuildConfig.BotVoiceChannel) == 0 ||
		len(Cfg.GuildConfig.GuildID) == 0 ||
		len(Cfg.GuildConfig.GuildName) == 0 ||
		len(Cfg.OwnerConfig.OwnerDiscordID) == 0 ||
		len(Cfg.GuildConfig.TextChannel) == 0 {
		return errNeedConfigure
	}

	if len(Cfg.DatabaseConfig.DatabaseConnectionString) == 0 {
		Cfg.DatabaseConfig.DatabaseConnectionString = defaultDB
	}

	if Cfg.DatabaseConfig.DatabaseType == 0 {
		Cfg.DatabaseConfig.DatabaseType = defaultDBType
	}

	err = database.ConnectToDB(Cfg.DatabaseConfig.DatabaseType, Cfg.DatabaseConfig.DatabaseConnectionString)
	if err != nil {
		return err
	}

	isPresent, err := database.IsInUserDB(database.User{DiscordID: Cfg.OwnerConfig.OwnerDiscordID})
	if err != nil {
		return err
	}

	if !isPresent {
		err = database.InsertUser(database.User{DiscordID: Cfg.OwnerConfig.OwnerDiscordID, Role: database.OWNER})
	} else {
		err = database.UpdateUser(database.User{DiscordID: Cfg.OwnerConfig.OwnerDiscordID, Role: database.OWNER})
	}

	if err != nil {
		return err
	}

	return nil
}

func dumpConfig(cfg *Configuration) error {
	fd, err := os.Create(cfgFileName)
	if err != nil {
		log.Println(err)
		return err
	}
	defer fd.Close()

	b, err := json.MarshalIndent(cfg, "", "  ")
	if err != nil {
		log.Println(err)
		return err
	}

	_, err = fd.Write(b)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}
